Chemisches Datenmodell für WebWriter:

Kurzanleitung:

Atoms.ts: 
Stellt Atome dar und gibt deren Eigenschaften wieder
Konstruktoraufruf: Name, ?Ladung
getter-Methoden für Eigenschaften:
        "name"
        "appearance"
        "atomic_mass"
        "boil"
        "category"
        "density"
        "discovered_by"
        "melt"
        "molar_heat"
        "named_by"
        "number"
        "period"
        "group"
        "phase" 
        "source"
        "bohr_model_image"
        "bohr_model_3d"
        "spectral_img"
        "summary"
        "symbol"
        "xpos"
        "ypos"
        "wxpos"
        "wypos"
        "shells"
        "electron_configuration"
        "electron_configuration_semantic"
        "electron_affinity"
        "electronegativity_pauling"
        "ionization_energies"
        "cpk-hex"
        "image"
        "block"

Molecules.ts
Stellt Moleküle dar.
Konstruktoraufruf:mindestens zwei Atome (mit Anzahl) sowie den Molekülname
Bsp: new Molecules([2,Hydrogen],[1,Oxygen],"Wasser")
new Molecules([1,Hydrogen],[1,Hydrogen],"Wasserstoff")
new Molecules([20,Carbon],[14,Hydrogen],"Phenolphthalein",[4,Oxygen])
Klasse berechnet selbstständig Atommasse und Ladung.
Kann Summenformel von einfachen Salzen selbstständig bestimmen. Komplexere Salze sowie andere Moleküle müssen selbstständig eingefügt werden über setFormular2()
Getter zu Attributen

Stoff.ts
Stellt Molekül oder Atom zum Experimentieren dar
Konstruktoraufruf: Atom oder Molekül - folgende Parameter optional:
_mass: number|undefined = undefined;
    _atomicMass: 
    _concentration:
    _amountSubstance: ;
    _volume: 
    _pKs_Value: 
    _pH_Value: 
    _standardReactionEnthalpy:

Klasse kann, wenn ausreichend Parameter vorhanden sind, andere Parameter selbst bestimmen.
Bestimmung ob starke oder schwache Säure eigenständig.

Methoden:
Berechnung pH-Wert (starke/schwache Säure): calculate_pH
Berechnung pOH-Wert: calculate_pOH
Berechnung pKs-Wert (schwache Säure): calculate_pKsWeakAcid

Indikator.ts
Stellt Indikatoren dar
Konstruktoraufruf: Name Indikator
4 Indikatoren vorhanden:
        "bromthymolblau",
        "methylrot",
        "phenolphthalein",
        "universalindikator"
Methoden:
Berechnung Farbe anhand pH-Wert: calculateColorFromPH

Reactions.ts
Stellt Reaktionen dar:
Konstruktoraufruf: Edukt[Stoff], Produkt[Stoff],?Indikator[Indikator] ?Umgebungsparameter
Eingabe von Edukuten/Produkten selbstständig

Methoden:
Berechnung Reaktionsenthalpie: calculateReactionEnthalpy
Berechnung Indikatorfarbe: getIndicatorColor

Tiration.ts:
Beta-Simulation Tiration:
Konstruktoraufruf: Erlenmeyerkoblen und Burette

Methoden:
Tropfen von Bürette in Erlenmeyerkoblen: oneDrop
