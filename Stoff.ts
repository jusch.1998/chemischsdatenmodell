import {Atom} from "./Atom"
import {Molecules} from "./Molecules"


class Stoff{

    //Properties
    _stoffMolecule: Atom|Molecules;
    _mass: number|undefined = undefined;
    _atomicMass: number|undefined = undefined;
    _concentration: number|undefined = undefined;
    _amountSubstance: number|undefined = undefined;
    _volume: number|undefined = undefined;
    _pKs_Value: number|undefined = undefined;
    _strongAcid: boolean|undefined = undefined;
    _pH_Value: number|undefined = undefined;
    _standardReactionEnthalpy: number|undefined = undefined
    

    //Constructor
    constructor(pStoff:Atom|Molecules, pMass:number|undefined = undefined, pAtomicMass:number|undefined = undefined, pConcentration:number|undefined = undefined,
        pAmountSubstance:number|undefined = undefined, pVolume:number|undefined = undefined, ppKs_Value:number|undefined = undefined, ppH_Value:number|undefined = undefined,
        pStandardReactionEnthalpy:number|undefined = undefined){
        this._stoffMolecule = pStoff;
        this._atomicMass = pStoff.getAtomicMass();
        pMass != undefined ? this.setMass(pMass) : this._mass = undefined;
        pConcentration != undefined ? this.setConcentration(pConcentration) : this._concentration = undefined;
        pAmountSubstance != undefined ? this.setAmountSubstance(pAmountSubstance) : this._amountSubstance = undefined;
        pVolume != undefined? this.setVolume(pVolume) : this._volume = undefined;
        this._pKs_Value = ppKs_Value;
        this._pH_Value = ppH_Value;
        this._standardReactionEnthalpy = pStandardReactionEnthalpy;

        if (ppKs_Value !== undefined){
            if (ppKs_Value < 4.7){
                this._strongAcid = true;
            }else{
                this._strongAcid = false;
            }
        }
        

    }

    //Accessors
    public getStoffMolecule(): Atom|Molecules{
        return this._stoffMolecule;
    }
    public getMass(): number|undefined{
        return this._mass;
    }
    public getAtomicMass(): number|undefined{
        return this._atomicMass;
    }
    public getConcentration(): number|undefined{
        return this._concentration;
    }
    public getAmountSubstance(): number|undefined{
        return this._amountSubstance;
    }
    public getVolume(): number|undefined{
        return this._volume;
    }
    public getpKs_Value(): number|undefined{
        return this._pKs_Value;
    }
    public getStrongAcid(): boolean|undefined{
            return this._strongAcid;
        }
    public getpH_Value(): number|undefined{
        return this._pH_Value;
    }
    public getStandardReactionEnthalpy(): number|undefined{
        return this._standardReactionEnthalpy;
    }


    public setStoffMolecule(pStoff:Atom|Molecules){
        this._stoffMolecule = pStoff;
    }
    
    public setMass(pMass:number){
        if (pMass >0){
            this._mass = pMass
        }
        else{
            throw new Error("Mass must be a positive number");
        }
        
    }
    public setConcentration(pConcentration:number){
        if (pConcentration >0){
            this._concentration = pConcentration;
        }else{
            throw new Error("Concentration must be a positive number");
        }
        
    }
    public setAmountSubstance(pAmountSubstance:number){
        if (pAmountSubstance >0){
            this._amountSubstance = pAmountSubstance;
        }else {
            throw new Error("Amount substance must be a positive number");
        }
    }
    public setVolume(pVolume:number){ 
        if (pVolume >0){
            this._volume = pVolume;
        }else{
            throw new Error("Volume must be a positive number");
        }
    }
    public setpKs_Value(ppKs_Value:number){
        this._pKs_Value = ppKs_Value;
    }
    public setpH_Value(ppH_Value:number){
        this._pH_Value = ppH_Value;
    }
    public setStrongAcid(){
        if (this.getpKs_Value() != undefined){
            //@ts-ignore
            if (this.getpKs_Value() < 4.7){
                this._strongAcid = true;
            }else{
                this._strongAcid = false;
            }
        }
    }
    public setStandardReactionEnthalpy(pStandardReactionEnthalpy:number){
        this._standardReactionEnthalpy = pStandardReactionEnthalpy;
    }



    //Methods

    public calculateMass():number{
        
        if (this.getAmountSubstance == undefined){
            throw new Error("Error: Amount Substance is not defined")
        }else{
            //@ts-ignore
            return this.getAmountSubstance() * this.getAtomicMass();                                //n = m/M --> n*M =m 
        }
    }

    public calculateAmountSubstancefromMass():number{
        if (this.getMass() == undefined){
            throw new Error("Error: Mass is not defined");
        }else{
            //@ts-ignore
            return this.getMass() / this.getAtomicMass();
        }
    }

    public calculateAmountSubstancefromConcentration():number{
        if (this.getConcentration() == undefined || this.getVolume == undefined){
            throw new Error("Error: Concentration or volume are not defined");
        }else{
            //@ts-ignore
            return this.getConcentration() / this.getAtomicMass();                                 // c = n/V --> c*V = n
        }
    }

    public calculateConcentration():number{

        if (this.getAmountSubstance() == undefined || this.getVolume() == undefined){
            throw new Error("Error: Amount substance or volume is not defined");
        }else{
            //@ts-ignore
            return this.getAmountSubstance() / this.getVolume();
        }
        
    }

    public calculateVolume():number{
        if (this.getAmountSubstance() == undefined || this.getConcentration() == undefined){
            throw new Error("Error: Amount substance or concentration is not defined");
        }else{
            //@ts-ignore
            return this.getAmountSubstance() / this.getConcentration();
        }
    }

    public calculate_pH():number{
        if (this.getStrongAcid() == true){
            if (this.getConcentration() == undefined) {
                throw new Error("Error: Concentration is not defined");
            }else
                //@ts-ignore
                return -Math.log(this.getConcentration());  //Negativer dekardischer Log der H30+-Konzentration. Aktivität nicht beachtet, spielt in der Schule keine Rolle
        }else{
            if (this.getStrongAcid() == false){
                if (this.getpKs_Value() == undefined || this.getConcentration() == undefined){ 
                    throw new Error("Error: pKs_Value or Concentration is not defined");
                }else
                    //@ts-ignore
                    return 0.5 * (this.getpKs_Value() - Math.log(this.getConcentration()));
            }else
            {
                throw new Error("Error: Strong Acid is not defined");
            }
        }
    }

    public calculate_pOH():number {
        let pH = this.calculate_pH();
        return 14 - pH;
    }

    public calculate_pKsWeakAcid():number{
        if(this.getStrongAcid() == true){
            throw new Error("Error: Acid is strong")
        }else{
            if (this.getpH_Value() == undefined || this.getConcentration() == undefined){
                throw new Error("Error: pH_Value or Concentration is not defined");
            }else
                //@ts-ignore
                return 2 * this.getpH_Value() + this.getConcentration()
        }

    }

}


export{Stoff}