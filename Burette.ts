import { Stoff } from "./Stoff";

class Burette{

    //Properties
    _titrator: Stoff;
    _maxVolume: number;
    _currVolume: number;

    //Constructor
    constructor(pTitrator:Stoff, pmaxVolume:number = 0.03){
        //@ts-ignore
        if(pTitrator.getVolume() == undefined){
            throw new Error("Volume ist not defined or is bigger than maxVolume");
        }else{
            //@ts-ignore
            if(pTitrator.getVolume() > pmaxVolume){
                throw new Error("Volume is bigger than maxVolume");
            }
        }
            
        this._titrator = pTitrator;
        this._maxVolume = pmaxVolume;
        //@ts-ignore
        this._currVolume = pTitrator.getVolume();
    }

    //Accessors
    public getTitrator(): Stoff{
        return this._titrator
    }
    public getMaxVolume(): number{
        return this._maxVolume;
    }
    public getCurrentVolume(): number{
        return this._currVolume;
    }

    public setTitrator(pTitrator:Stoff){
        this._titrator = pTitrator
    }
    public setMaxVolume(pmaxVolume:number){
        this._maxVolume = pmaxVolume;
    }
    public setCurrentVolume(pcurrVolume:number){
        this._currVolume = pcurrVolume;
    }

    //Methods
}

export default Burette;