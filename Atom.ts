import "./periodic-table-lookup.json"
import jsonData from "./periodic-table-lookup.json";
//
class Atom{

    // Properties
    readonly _atom: object;
    _ionicCharge:number;
    _ionicType: string

    // Constructor
    constructor(pName:string, pIonicCharge:number = 0) {
        const jsonData= require('./periodic-table-lookup.json');
        this._atom = jsonData[pName];
        this._ionicCharge = pIonicCharge;

        if(pIonicCharge == 0){
            this._ionicType = ""
        }
        else if(pIonicCharge > 0){
            this._ionicType = "Kation"
        }else {
            this._ionicType = "Anion"
        }
    }

    // Accessors
    public getProtons():number{
        // @ts-ignore
        return this._atom["number"];
    };
    public  getElectrons():number{
        // @ts-ignore
        return this._atom["number"];
    };
    public  getNeutrons():number{
        // @ts-ignore
        return this._atom["number"];
    };
    public  getNumber():number{
        // @ts-ignore
        return this._atom["number"]
    }
    public getAtomicMass():number{
        // @ts-ignore
        return this._atom["atomic_mass"];
    };
    public getDensity():number{
        // @ts-ignore
        return this._atom["density"]
    };
    public getBoil():number{
        // @ts-ignore
        return this._atom["boil"]
    };
    public getCategory():string{
        // @ts-ignore
        return this._atom["category"]
    };
    public getMelt():number{
        // @ts-ignore
        return this._atom["melt"]
    };
    public getMolarHeat():number{
        // @ts-ignore
        return this._atom["molar_heat"]
    };
    public getPeriod():number{
        // @ts-ignore
        return this._atom["period"]
    };
    public getGroup():number{
        // @ts-ignore
        return this._atom["group"]
    };
    public getPhase():string{
        // @ts-ignore
        return this._atom["phase"]
    };
    public getElectronConfiguration():string{
        // @ts-ignore
        return this._atom["electron_configuration"]
    };
    public getElectronConfigurationSemantic():string{
        // @ts-ignore
        return this._atom["electron_configuration_semantic"]
    };
    public getElectronAffinity():number{
        // @ts-ignore
        return this._atom["electron_affinity"]
    };
    public getElectronNegativityPauling():number{
        // @ts-ignore
        return this._atom["electronegativity_pauling"]
    };
    public getBlock():string{
        // @ts-ignore
        return this._atom["block"]
    };
    public getSymbol():string{
        // @ts-ignore
        return this._atom["symbol"]
    };
    public getDiscoveryBy():string{
        // @ts-ignore
        return this._atom["discovered_by"]
    };
    public getNamedBy():string{
        // @ts-ignore
        return this._atom["named_by"]
    };
    public getSource():string{
        // @ts-ignore
        return this._atom["source"]
    };
    public getBohrModelImage():string{
        // @ts-ignore
        return this._atom["bohr_mode_image"]
    };
    public getBohrModel3D():string{
        // @ts-ignore
        return this._atom["bohr_model_3d"]
    };
    public getSpectralImg():string{
        // @ts-ignore
        return this._atom["spectral_img"]
    };
    public getSummary():string{
        // @ts-ignore
        return this._atom["summery"]
    };
    public getYpos():number{
        // @ts-ignore
        return this._atom["ypos"]
    };
    public getWXpos():number{
        // @ts-ignore
        return this._atom["wxpos"]
    };
    public getWYpos():number{
        // @ts-ignore
        return this._atom["wypos"]
    };
    public getXpos():number{
        // @ts-ignore
        return this._atom["xpos"]
    };
    public getShells():[number]{
        // @ts-ignore
        return this._atom["shells"]
    };
    public getIonizationEnergies():[number]{
        // @ts-ignore
        return this._atom["ionization_energies"]
    };
    public getCPKHEX():string{
        // @ts-ignore
        return this._atom["cpk-hex"]
    };
    public getImage():object{
        // @ts-ignore
        return this._atom["image"]
    };
    public getIonicType():string{
        return this._ionicType;
    }
    public getIonicCharge():number{
        return this._ionicCharge;
    }
    

    public setIonicCharge(pCharge:number):void{
        this._ionicCharge = pCharge;

        if(pCharge == 0){
            this._ionicType = ""
        }
        else if(pCharge > 0){
            this._ionicType = "Kation"
        }else {
            this._ionicType = "Anion"
        }

    }
    // Methods

}

export {Atom}
