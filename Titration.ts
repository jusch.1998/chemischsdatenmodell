import Burette from "./Burette";
import ErlenmeyerFlask from "./ErlenmeyerFlask";

class Titration {

    //Properties
    _burette: Burette;
    _flask: ErlenmeyerFlask;

    //Constructor
    constructor(pBurette: Burette, pFlask: ErlenmeyerFlask) {
        this._burette = pBurette;
        this._flask = pFlask;
    }

    //Accessors
    public getBurette(): Burette {
        return this._burette;
    }
    public getFlask(): ErlenmeyerFlask {
        return this._flask;
    }

    public setBurette(pBurette: Burette) {
        this._burette = pBurette;
    }
    public setFlask(pErlenmeyerFlask: ErlenmeyerFlask) {
        this._flask = pErlenmeyerFlask
    }

    //Methods

    //one Drop is 0.05mL
    public oneDrop(){
        this.getBurette().setCurrentVolume(this.getBurette().getCurrentVolume() - 0.05)
        this.getFlask().setCurrentVolume(this.getFlask().getCurrentVolume() + 0.05)

        
    }
}