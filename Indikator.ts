import "./indicator_table.json";

class Indikator{
    
    //Properties
    _indicator:object;

    //Constructor
    constructor(pName:string){
        const jsonData= require('./indicator_table.json');
        if (jsonData[pName] != null){
            this._indicator = jsonData[pName];
        }else{
            throw new Error("Indikator nicht gefunden");
        }
        
    }

    //Accessors
    public getIndicator():object{
        return this._indicator;
    }

    public getUmschlagspunkt():[number]{
        // @ts-ignore 
        return this._indicator["pH_Umschlagspunkt"];
    }
    public getpHFarben():[string]{
        // @ts-ignore
        return this._indicator["pH_Farben"];
    }

    public getName():string{
        //@ts-ignore
        return this._indicator["name"]
    }
    public getAtomicMass():string{
        //@ts-ignore
        return this._indicator["atomicMass"]
    }
    public getFormular():string{
        //@ts-ignore
        return this._indicator["formular"]
    }

    public setIndikator(pIndikator:string):void{
        const jsonData= require('./indicator_table.json');
        if (jsonData[pIndikator] != null){
            this._indicator = jsonData[pIndikator];
        }else{
            throw new Error("Indikator nicht gefunden");
        }
    }

    //Methods

    public calculateColorFromPH(ppH:number):string{
        const pH_Umschlagspunkt = this.getUmschlagspunkt();

        let ph_Farbe = "";
        
        for (let i  = 0; i< pH_Umschlagspunkt.length;i++){
            if (ppH <= pH_Umschlagspunkt[i]){
                ph_Farbe = this.getpHFarben()[i];
                break;
            }
        }

        return ph_Farbe;
    }



}

export{Indikator}