import {Atom} from "./Atom";

class Molecules {

    //Properties
    _atom1: [number,Atom];
    _atom2: [number,Atom];
    _argsAtom: [number, Atom][];

    _formula: string = "";
    _ionicCharge: number;
    _atomicMass!: number;
    _name: string;
    //_standardReactionEnthalpy: number;

    //TODO
    //PKS PKB
    // Constructor

    constructor(pAtom1:[number,Atom], pAtom2:[number,Atom], pName:string, ...args:[number,Atom][]) {
        this._atom1 = pAtom1;
        this._atom2 = pAtom2;
        this._argsAtom = args;
        this._name = pName;

        this._ionicCharge = this._atom1[1].getIonicCharge() * this._atom1[0] + this._atom2[1].getIonicCharge() * this._atom2[0];
        if (args != null){
            for (let i = 0; i < args.length;i++){
                this._ionicCharge += args[i][1].getIonicCharge() * args[i][0];
            }
        }



        this.setAtomicMass()
        this.setFormula()

    }
            
    

    // Accessors
    public getAtom1():Atom{
        return this._atom1[1];
    }
    public getAtom2():Atom{
        return this._atom2[1];
    }
    public getArgsAtom():[number,Atom][]{
        return this._argsAtom;
    }
    setFormula(){


        let allKations: [number, Atom][] = []
        let allAnions: [number, Atom][] = []


        for (let i = 0; i < this._argsAtom.length; i++){
            if (this._argsAtom[i][1].getIonicType() === "Kation"){
                allKations.push(this._argsAtom[i])
            }else if (this._argsAtom[i][1].getIonicType() === "Anion"){
                allAnions.push(this._argsAtom[i])
            }
        }


        if (this._atom1[1].getIonicType() === "Kation"){
            allKations.push(this._atom1)
        }else if (this._atom1[1].getIonicType() === "Anion"){
            allAnions.push(this._atom1)
        }

        if (this._atom2[1].getIonicType() === "Kation"){
            allKations.push(this._atom2)
        }else if (this._atom2[1].getIonicType() === "Anion"){
            allAnions.push(this._atom2)
        }


        let kationOrderd = new Array(allKations.length)
        //setzt aktuell das Element an die passende Number --> Leerstellen
        for (let i = 0; i < allKations.length; i++){
            kationOrderd[allKations[i][1].getNumber()-1] = allKations[i]
        }
        kationOrderd = kationOrderd.filter(n=>n)


        let anionOrderd = new Array(allAnions.length)
        for (let i = 0; i < allAnions.length; i++){
            anionOrderd[allAnions[i][1].getNumber()] = allAnions[i]
        }
        anionOrderd = anionOrderd.filter((n=>n))

        for (let i = 0; i <  kationOrderd.length; i++){

            this._formula += kationOrderd[i][1].getSymbol()
            this._formula += kationOrderd[i][0] == 1 ? "" : kationOrderd[i][0];
        }

        for (let i = 0; i <  anionOrderd.length; i++){
            this._formula += anionOrderd[i][1].getSymbol() + anionOrderd[i][0]
        }
    }
    public setFormular2(pFormula: string):void{
        this._formula = pFormula;
    }
    public getFormular():string{
        return  this._formula;
    }
    public getIonicCharge():number{
        return this._ionicCharge;
    }

    public setAtomicMass(){

        let atomicMass1 : number = this.getAtom1().getAtomicMass() * this._atom1[0];
        let atomicMass2 : number = this.getAtom2().getAtomicMass() * this._atom2[0];
        let atomicMassN : number = 0;
        
        for (let i =0; i < this.getArgsAtom.length;i++){
            atomicMassN += this.getArgsAtom()[i][1].getAtomicMass() + this.getArgsAtom()[i][0]
        }

        this._atomicMass = atomicMass1 + atomicMass2 + atomicMassN;


    }
    public getAtomicMass():number{
        return this._atomicMass;
    }

    // Methods


}

export{Molecules}
