import { Atom } from "./Atom";
import { Indikator } from "./Indikator";
import { Molecules } from "./Molecules";
import { Stoff } from "./Stoff";

class Reaction{

    //Properties

    _educt : [Stoff];
    _product : [Stoff];
    _indicator: Indikator|undefined = undefined;
    _umgebung:[string];

    //Constructor
    constructor(pEduct:[Stoff],pProduct:[Stoff], pIndicator:Indikator|undefined = undefined,pUmgebung:[string]){
        this._educt = pEduct;
        this._product = pProduct;
        this._indicator = pIndicator;
        this._umgebung = pUmgebung;
    }

    //Accessors
    public getEduct():[Stoff] {
        return this._educt;
    }
    public getProduct():[Stoff] {
        return this._product;
    }
    public getIndicator():Indikator|undefined {
        return this._indicator;
    }
    public getUmgebung():[string]{
        return this._umgebung
    }

    public setEduct(pEduct:[Stoff]):void {
        this._educt = pEduct;
    }
    public setProduct(pProduct:[Stoff]):void {
        this._product = pProduct;
    }
    public setIndicator(pIndicator:Indikator|undefined):void {
        this._indicator = pIndicator;
    }
    public setUmgebung(pUmgebung:[string]):void{
        this._umgebung = pUmgebung
    }
    //Methods

    public calculateReactionEnthalpy():number {
        let reactionEnthalpy = 0;
        for (let i = 0; i < this._educt.length; i++) {
            if (this.getEduct()[i].getStandardReactionEnthalpy() == undefined){
                throw new Error("Reaction Enthalpy of Educt " + i + " is undefined");
            }
            else{
                //@ts-ignore
                reactionEnthalpy += this.getEduct()[i].getStandardReactionEnthalpy();
            }
        }

        return 0;
    }

    public getIndicatorColor(ppH:number):string {
        if (this.getIndicator() == undefined){
            throw new Error("Indicator is undefined");
        }else{
            //@ts-ignore
            return this.getIndicator().getUmschlagspunkt(ppH);
        }
    }

}

export default Reaction;