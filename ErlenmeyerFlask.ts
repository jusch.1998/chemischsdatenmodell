import { Stoff } from "./Stoff";

class ErlenmeyerFlask{

    //Properties
    _maxVolume: number;
    _currVolume: number;
    _chemicals: [Stoff|undefined]
    _concentration: number | undefined;


    //Constructor
    constructor(pmaxVolume: number = 0.25, pcurrVolume: number, pchemicals: [Stoff|undefined]){
        if (pcurrVolume > pmaxVolume){
            throw new Error("Current volume cannot be greater than maximum volume")
        }
        this._maxVolume = pmaxVolume;
        this._currVolume = pcurrVolume;
        this._chemicals = pchemicals;
    }
    

    //Accessors
    public getMaxVolume(): number{
        return this._maxVolume;
    }
    public getCurrentVolume(): number{
        return this._currVolume;
    }
    public getChemicals(): [Stoff|undefined] {
        return this._chemicals;
    }

    public setMaxVolume(pmaxVolume: number): void{
        this._maxVolume = pmaxVolume;
    }
    public setCurrentVolume(pcurrVolume: number): void{
        this._currVolume = pcurrVolume;
    }
    public setChemicals(pchemicals: [Stoff|undefined]): void {
        this._chemicals = pchemicals;
    }

    //Methods
}

export default ErlenmeyerFlask;